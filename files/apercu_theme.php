<?php
session_start();
require("./../config/config.php");
include ("./class/class.inc.php");
include ("./secure.inc.php");
include ("fun.inc.php");
if (isset($_POST['id_crs'])) {
    $_SESSION['sel_crs'] = $_POST['id_crs'];
}
$pdo = new Mypdo;
$crs_info = $pdo->query("SELECT * FROM t_cours WHERE id_crs = \"" . $_SESSION['sel_crs'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecole-facile :: <?php echo $crs_info[0]['nom_crs'] ?></title>
        <!-- Bootstrap core CSS -->
        <link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.css" rel="stylesheet" media="screen">
        <script src="<?php echo ROOT; ?>./plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class='container'>
            <div class="header">
                <ul class="nav nav-pills pull-right">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./logout.php">Déconnexion</a></li>
                </ul>
                <h3 class="text-muted"><?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></h3>
            </div>
            <div class="row">
                <h2>Liste des thèmes</h2>
                <ul class="list-group">
                    <?php
                    $liste_theme = liste_theme($pdo, $_SESSION['sel_crs']);
                    echo $liste_theme;
                    ?>
                </ul>
            </div>
            <div class="footer">
                <p>Portail <?php echo NAME; ?> - Version <?php /* echo git_version(); */ ?></p>
            </div>
        </div>
    </body>
</html>
