<?php

class Projet{

    //Attributs
    protected $id;
    protected $tab_type;
    protected $table_name;
    protected $suffix;
    public $pdo;

    /**
     * @author David Turberg, Julien Trolliet
     * @version 1.0
     * 
     * @param Object $pdo Instace de mypdo
     * @param int $id l'id du groupe
     * 
     * @tutorial
     * Initialise les attributs parents et ceux de la class si l'id est pr�cis�.
     */
    function __construct($id = 0) {
        $this->pdo = new Mypdo();
        $this->id = $id;
       
    }

    /**
     * @author David Turberg, Julien Trolliet
     * @version 2.0
     * 
     * @tutorial
     * R�cup�re les type de donn�es dans les champs de la base de donn�es.
     */
    function init_tab_type() {
        $query = ("SELECT DISTINCT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name =\"" . $this->table_name . "\"");
        $result = $this->pdo->query($query)->fetchAll();
        foreach ($result as $line) {
            $this->tab_type[$line['COLUMN_NAME']] = $line['DATA_TYPE'];
        }
    }

    /**
     * @author David Turberg, Julien Trolliet
     * @version 1.0
     * 
     * @param array $tab
     * @tutorial
     * Formate les donn�es re�u du tableau $tab et les envoies � la fonction insert.
     */
    function add($tab) {
		$this->init_tab_type();
//        print_r($tab);
        $tab_keys = array_keys($tab);
        $tab_values = array_values($tab);
        //Liste de tout les types qui requi�re des " "
        $list_string = array("char", "varchar", "tinytext", "text", "mediumtext", "longtext", "binary", "varbinary", "tinyblob", "mediumblob", "blob", "longblob", "enum", "set", "datetime","date","time");
        for ($i = 0; $i < sizeof($tab); $i++) {
            if (array_key_exists($tab_keys[$i], $this->tab_type)) {
                if (in_array($this->tab_type[$tab_keys[$i]], $list_string)) { //Si le type de champ figure dans $list_string, ajouter des ""
                    $tab_values[$i] = "\"" . $tab_values[$i] . "\"";
                }
                if ($tab_values[$i] == "") {
                    unset($tab_keys[$i]);
                    unset($tab_values[$i]);
                }
            } else {
                unset($tab_keys[$i]);
                unset($tab_values[$i]);
            }
        }

        $id = $this->pdo->insert($this->table_name, $tab_keys, $tab_values,"",0);
        return($id);
    }

    /**
     * @author David Turberg, Julien Trolliet
     * @version 2.0
     * 
     * @param array $tab 
     */
    function suppr($tab) {
        $tab = $this->pdo->delete($this->table_name, ' WHERE id' . $this->suffix . ' = ' . $tab['id' . $this->suffix . ''],0);
    }

    /**
     * @author David Turberg, Julien Trolliet
     * @version 2.0
     * 
     * @param array $tab 
     */
    function disable($tab) {
        $tab = $this->pdo->update($this->table_name, array('actif' . $this->suffix), array("0"), "WHERE id" . $this->suffix . "=" . $tab['id' . $this->suffix . ""]);
    }

      /**
     * @author David Turberg
     * @version 2.0
     * 
     * @param array $tab
     * @tutorial
     * Formate les donn�es re�u du tableau $tab et les envoies � la fonction update.
     */
    function modif($tab) {
        if (isset($tab['password_per'])) {
            if ($tab['password_per'] != NULL) {
                $tab['password_per'] = md5(htmlentities("456456ght45" . $tab['password_per'] . "Zt5464frt35"));
                //$tab['password_per'] = mhash(MHASH_SHA1,htmlentities("#45*".$tab['password_per']."Zt&ww"));
            } else {
                $per = new personne($this->sql, $tab['id_per']);
                $tab['password_per'] = $per->get_password();
            }
        }
        $tab_keys = array_keys($tab);
        $tab_values = array_values($tab);
	//	print_r($tab_keys);
	//	print_r($tab_values);
        $list_string = array("char", "varchar", "tinytext", "text", "mediumtext", "longtext", "binary", "varbinary", "tinyblob", "mediumblob", "blob", "longblob", "enum", "set", "datetime", "date", "time");

        for ($i = 0; $i < sizeof($tab); $i++) {
            if (array_key_exists($tab_keys[$i], $this->tab_type)) {
                if (in_array($this->tab_type[$tab_keys[$i]], $list_string)) {
                    $tab_values[$i] = "\"" . $tab_values[$i] . "\"";
                }
            } else {
                unset($tab_keys[$i]);
                unset($tab_values[$i]);
            }
        }
        $tab = $this->pdo->update($this->table_name, $tab_keys, $tab_values, 'WHERE id' . $this->suffix . ' = ' . $tab['id' . $this->suffix . ''], 0);
    }

	public function get_date_week($date){
		$time=strtotime($date);
		$num_jour = date("N",$time)-1;
		$jour = date("d",$time);
		$mois = date("m",$time);
		$annee = date("Y",$time);
		$date_week['db'] = date("Y-m-d",mktime(0,0,0,$mois,$jour-$num_jour,$annee));
		$date_week['fin'] = date("Y-m-d",mktime(0,0,0,$mois,$jour+(7-$num_jour),$annee));
		return $date_week;
	}

    function date_fr($date_time) {
        $mois = array("", "janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");
        $date_fr = (substr($date_time, 8, 2) * 1) . " " . strtolower($mois[(substr($date_time, 5, 2) * 1)]) . " " . (substr($date_time, 0, 4) * 1);
        return($date_fr);
    }

    function date_jma($date_time) {
        $date_time = substr($date_time, 0, 10);
        $date_time = substr($date_time, 8, 2) . "." . substr($date_time, 5, 2) . "." . substr($date_time, 0, 4);
        return($date_time);
    }
    
    function date_eu($date_time) {
        $date_time = substr($date_time, 0, 10);
        $date_time = substr($date_time, 8, 2) . "/" . substr($date_time, 5, 2) . "/" . substr($date_time, 0, 4);
        return($date_time);
    }

    function date_time_fr($date_time) {
        $mois = array("", "janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre");
        $date_fr = $this->jour_fr($date_time) . " " . $this->date_fr($date_time) . " " . date("H:i", strtotime($date_time));
        return($date_fr);
    }

    function date_time_fr_html($date_time) {
        $date_fr = ($this->date_time_fr($date_time));
        return($date_fr);
    }

    function date_fr_to_en($date) {
        if ($date != "0000-00-00" && $date != "") {
            $tab = explode("-", $date);
            $date = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
        } else {
            $date = "";
        }
        return $date;
    }

    function date_en_to_fr($date) {
        if ($date != "0000-00-00" && $date != "") {
            $tab = explode("-", $date);
            $date = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
        } else {
            $date = "";
        }
        return $date;
    }

    function date_time_fr_to_en($date) {
        if ($date != "0000-00-00" && $date != "") {
            $time = explode(" ", $date);
            echo $time[1];
            $tab = explode("-", $time[0]);
            echo $date = $tab[2] . "-" . $tab[1] . "-" . $tab[0] . " " . $time[1];
        } else {
            $date = "";
        }
        return $date;
    }

    function date_time_en_to_fr($date) {
        if ($date != "0000-00-00" && $date != "") {
            $time = explode(" ", $date);
            $tab = explode("-", $date);
            $date = $tab[2] . "-" . $tab[1] . "-" . $tab[0] . " " . $time[1];
        } else {
            $date = "";
        }
        return $date;
    }

    function time_fr($time) {
        $time_fr = date("H:i", strtotime($time));

        return($time_fr);
    }

    function date_time_vcal($date_time) {
        $date_time = date("Y-m-d H:i:s", strtotime($date_time) - 3600);
        $date_time = str_replace("-", "", $date_time);
        $date_time = str_replace(" ", "T", $date_time);
        $date_time = str_replace(":", "", $date_time);
        return($date_time);
    }

    function date_mysql_to_fr($date_mysql) {
        list($annee, $mois, $jour) = explode("-", $date_mysql);
        return $jour . "-" . $mois . "-" . $annee;
    }

    function date_fr_to_mysql($date_fr) {
        list($jour, $mois, $annee) = explode("-", $date_fr);
        return $annee . "-" . $mois . "-" . $jour;
    }

    function jour_fr($date_time) {
        $jour = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
        return($jour[date("w", strtotime($date_time))]);
    }




    /**
     * @author David Turberg
     * @version 1.0
     *
     * @param string $var 
     * 
     * @tutorial
     * Initialise l'attribut avec la valeur pass� en argument.
     */
    function set_id($var) {
        $this->id = $var;
    }

    /**
     * @author David Turberg
     * @version 1.0
     *
     * @return string 
     * @tutorial
     * Retour la valeur de l'attribut.
     */
    function get_id() {
        return($this->id);
    }



}

?>