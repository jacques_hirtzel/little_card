<?php

class Mypdo extends PDO {

    /**
     * Instance de la classe PDO
     *
     * @var PDO
     * @access private
     */
    public $PDOInstance = null;
    
    /**
     * Constructeur
     *
     * @param void
     * @return void
     * @see PDO::__construct()
     * @access private
     */
    function __construct() {

        try {
            $this->PDOInstance = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (Exception $e) {
            die('ERREUR SQL :'.$e->getMessage());
        }
		return ($this->PDOInstance);
    }

    /**
     * Crée et retourne l'objet SPDO
     *
     * @access public
     * @static
     * @param void
     * @return SPDO $instance
     */
    public static function getInstance() {
        if (is_null(self::$instance)) {
            self::$instance = new PDO('mysql:dbname=' . BASE_NAME . ';host=' . SQL_HOST, SQL_USER, SQL_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        }
        return self::$instance;
    }

    /**
     * Exécute une requête SQL avec PDO
     *
     * @param string $query La requête SQL
     * @return PDOStatement Retourne l'objet PDOStatement
     */
    public function query($query, $debug=false) {
        if ($debug) {
            echo "<br/>";
            echo $query;
            echo "<br/>";
        }
        return $this->PDOInstance->query($query);
    }


	public function prepare($query, $debug=false) {
        if ($debug) {
            echo "<pre>";
            echo $query;
            echo "</pre>";
        }
        return $this->PDOInstance->prepare($query);
    }
//return la derniere id inserer
    public function last_insert_id() {
		return $this->PDOInstance->lastInsertId();
    }

    public function execute($exec, $debug=false) {
        if ($debug) {
            echo "<br/>";
            echo print_r($exec);
            echo "<br/>";
        }
        return $this->query($exec);
    }

    function select($table, $champs, $argument='', $debug=false) {

        $result = array();
        $query = "SELECT $champs FROM $table $argument";
        if ($debug) {
            echo $query;
        }
        $result = $this->query($query, $debug);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $tab = $result->fetchAll();
        return ($tab);
    }

    /**
     * Mis Ã  jour d'un enregistrement
     * 
     * @param type $table Nom de la table
     * @param type $champs Nom de champs Ã  modifier
     * @param type $newValeur Nouvelles valeurs de champs
     * @param type $argument Arguments
     */
    function update($table, $champs, $newValeur, $argument, $debug=false) {
        
		$str = "";
		$i=0;
			//$debug = true;
		if(is_array($champs)){
			foreach($champs AS $champ){
				$str .= $champ."=".$newValeur[$i].",";
				$i++;
			}
			$str = substr($str,0,-1);
		}
        $statement = "UPDATE $table SET $str $argument";
        if ($debug) {
            echo $statement;
        }
        return $this->execute($statement,$debug);
    }

    /**
     * insert
     * 
     * @param type $table Nom de la table
     * @param type $Colonne Nom de champs Ã  modifier
     * @param type $Valeur Nouvelles valeurs de champs
     */
    function insert($table, $colonne, $valeur, $argument="", $debug=false) {

        
        if(is_array($colonne)){
            $colonne = implode(",",$colonne);
            $valeur = implode(",",$valeur);
        }
        $statement = "INSERT INTO $table ( $colonne ) VALUES ( $valeur ) $argument";
        if ($debug) {
            echo $statement;
        }
		//$this->prepare(
        $this->execute($statement);
		return $this->last_insert_id();
    }

    function copy_table($table, $argument="", $debug=false) {


        $statement = "INSERT INTO $table $argument";
        if ($debug) {
            echo $statement;
        }
        $this->execute($statement);
    }

    function delete($table, $argument, $debug=false) {
        $statement = "DELETE FROM $table $argument";
        if ($debug) {
            echo $statement;
        }
        $this->execute($statement);
    }

    /* new_obj
     * création d'un objet de classe $class_name extrait de la table 
     * t_$class_name en fonction de l'id $id
     */

    function new_obj($class_name, $id, $debug=false) {

        // recherche de la clef primaire
        $query = "SHOW COLUMNS FROM  t_" . $class_name . " WHERE  `Key` =  'PRI'";
        $result = $this->query($query);
        $tab_pk = $result->fetch();
        // print_r($tab_pk);
        // nom du cmaps de la clef primaire
        $field_pk = $tab_pk['Field'];

        // Recherche de l'enregistrement correspondant à l'id reçu
        $query = "SELECT * FROM t_" . $class_name . " where " . $field_pk . "=" . $id;
        if ($debug) {
            echo $query;
        }
        $result = $this->query($query);

        // tableau des arguments à passer au constructeur de la classe
        // ici on lui passe l'objet pdo
        $args = Array($this);

        // création de l'objet à partir des index
        $obj = $result->fetchObject($class_name, $args);

        // retour de l'objet
        return $obj;
    }

// new-obj
}
?>
