<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Eleve
 *
 * @author CP-10DRO
 */
Class User extends Projet{    

    protected $nom;
    protected $prenom;
    protected $email;   
    protected $password;

    function __construct($id=0){  
        $this->table_name = "t_users";
        $this->suffix = "_usr";
        parent::__construct($id);
        if($id){
            $this->init();           
        }
    }

    function init(){     
        $query = "SELECT * FROM ".$this->table_name." WHERE id_usr=".$this->id;
        $tab = $this->pdo->query($query)->fetch(PDO::FETCH_ASSOC);
        $this->nom = $tab['nom_usr'];
        $this->prenom = $tab['prenom_usr'];
        $this->email = $tab['email_usr'];
        $this->password = $tab['password_usr'];
    }
    

	function gen_passwd(){
		$passwd = substr(md5(date("ssmnss")."756847$$$".date("Ydsss")),0,8);
		$this->set_password(md5(htmlentities("456456ght45".$passwd."Zt5464frt35")));
//		$this->set_password(md5(htmlentities("456456ght45"."test"."Zt5464frt35")));
		$query = "UPDATE t_usrsonnes SET password_usr=\"".$this->get_password()."\" WHERE id_usr=".$this->get_id();
		$this->pdo->query($query);
		return($passwd);
	}

    public function get_nom() {
        return $this->nom;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }

    public function get_prenom() {
        return $this->prenom;
    }

    public function set_prenom($prenom) {
        $this->prenom = $prenom;
    }

    public function get_telephone() {
        return $this->telephone;
    }

    public function set_telephone($telephone) {
        $this->telephone = $telephone;
    }

    public function get_email() {
        return $this->email;
    }

    public function set_email($email) {
        $this->email = $email;
    }

    public function get_password() {
        return $this->password;
    }

    public function set_password($password) {
        $this->password = $password;
    }

	 public function get_actif() {
        return $this->actif;
    }

    public function set_actif($actif) {
        $this->actif = $actif;
    }
}
?>
