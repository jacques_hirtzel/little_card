<?php

function create_pdo($type, $host, $name, $login, $pswd) {
    try {
        $pdo = new PDO("" . $type . ":host=" . $host . ";dbname=" . $name . "", "" . $login . "", "" . $pswd . "");
        $pdo->exec("SET CHARACTER SET utf8");
    } catch (Exception $e) {
        $eReturn = "Erreur : " . $e->getMessage() . "<br/>\n";
        $eReturn .= "Erreur : " . $e->getCode() . "<br/>";
        return $eReturn;
    }
    return $pdo;
}

function option_langue($pdo) {
    $langue = $pdo->query("SELECT id_lng, nom_lng FROM t_langues")->fetchAll(PDO::FETCH_ASSOC);
    $return = "\n";
    foreach ($langue AS $value) {
        $return .= "\t\t\t\t\t<option value=\"" . $value['id_lng'] . "\">" . $value['nom_lng'] . "</option>\n";
    }
    $return .= "";
    return $return;
}

function option_cours($pdo) {
    $cours = $pdo->query("SELECT id_crs, nom_crs FROM t_cours")->fetchAll(PDO::FETCH_ASSOC);
    $return = "\n";
    foreach ($cours AS $value) {
        $return .= "\t\t\t\t<option value=\"" . $value['id_crs'] . "\">" . $value['nom_crs'] . "</option>\n";
    }
    return $return;
}

function option_sens($pdo) {
    $sens = $pdo->query("SELECT id_sns, sens_sens FROM t_sens")->fetchAll(PDO::FETCH_ASSOC);
    $return = "\n";
    foreach ($sens AS $value) {
        $return .= "\t\t\t\t\t\t<option value=\"" . $value['id_sns'] . "\">" . $value['sens_sens'] . "</option>\n";
    }
    return $return;
}

function option_theme($pdo, $id_crs) {
    $themes = $pdo->query("SELECT id_thm, nom_thm FROM t_themes WHERE id_crs = \"" . $id_crs . "\"")->fetchAll(PDO::FETCH_ASSOC);
    $return = "";
    foreach ($themes AS $value) {
        $return .= "\t\t\t\t\t\t<option value=\"" . $value['id_thm'] . "\">" . $value['nom_thm'] . "</option>\n";
    }
    return $return;
}

function liste_crs($pdo, $id) {
    $cours = $pdo->query("SELECT * FROM t_cours")->fetchAll(PDO::FETCH_ASSOC);
    $return = "\n";
    foreach ($cours as $value) {
        $return .= "\t\t\t<li class=\"list-group-item\">\n";
        $return .= "\t\t\t\t<form role=\"form\" method=\"post\" action=\"apercu_theme.php\">\n";
        $return .= "\t\t\t\t\t<input type=\"hidden\" value=\"" . $value['id_crs'] . "\" name=\"id_crs\">\n";
        $return .= "\t\t\t\t\t<input type=\"submit\" value=\"" . $value['nom_crs'] . "\" class=\"btn btn-default\">\n";
        $return .= "\t\t\t\t</form>\n";
        if ($id == $value['id_usr']) {
            $return .= "\t\t\t\tMon cours!\n";
        }
        $return .= "\t\t\t</li>\n";
    }
    return $return;
}

function liste_theme($pdo, $id_crs) {
    $themes = $pdo->query("SELECT * FROM t_themes WHERE id_crs = \"" . $id_crs . "\"");
    $return = "\n";
    foreach ($themes AS $value) {
        $return .= "\t\t\t<li class=\"list-group-item\">\n";
        $return .= "\t\t\t\t<form role=\"form\" method=\"post\" action=\"apercu_voc.php\">\n";
        $return .= "\t\t\t\t\t<input type=\"hidden\" value=\"" . $value['id_thm'] . "\" name=\"id_thm\">\n";
        $return .= "\t\t\t\t\t<input type=\"submit\" value=\"" . $value['nom_thm'] . "\" class=\"btn btn-default\">\n";
        $return .= "\t\t\t\t</form>\n";
        $return .= "\t\t\t</li>\n";
    }
    $return .= "\n";
    return $return;
}

function liste_voc($pdo, $id_thm) {
    $sens = $pdo->query("SELECT * FROM t_sens NATURAL JOIN t_vocabulaires WHERE id_thm = \"" . $id_thm . "\"")->fetchAll(PDO::FETCH_ASSOC);
    $return = "\n";
    foreach ($sens AS $value) {
        $mot1 = $pdo->query("SELECT * FROM t_mots WHERE id_mot = \"" . $value['id_mot_1'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
        $mot2 = $pdo->query("SELECT * FROM t_mots WHERE id_mot = \"" . $value['id_mot_2'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
        $return .= "\t\t\t<tr>\n";
        $return .= "\t\t\t\t<td>" . $mot1[0]['mot_mot'] . "</td>\n";
        $return .= "\t\t\t\t<td>" . $mot2[0]['mot_mot'] . "</td>\n";
        $return .= "\t\t\t\t<td>" . $value['sens_sens'] . "</td>\n";
        $return .= "\t\t\t</tr>\n";
    }
    return $return;
}
