<?php
session_start();
require("./../config/config.php");
include ("./class/class.inc.php");
include ("./secure.inc.php");
include ("fun.inc.php");
if (isset($_POST['id_thm'])) {
    $_SESSION['sel_thm'] = $_POST['id_thm'];
}
$pdo = new Mypdo;
$crs_info = $pdo->query("SELECT * FROM t_cours WHERE id_crs = \"" . $_SESSION['sel_crs'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
$thm_info = $pdo->query("SELECT * FROM t_themes WHERE id_thm = \"" . $_SESSION['sel_thm'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
$langue = $pdo->query("SELECT * FROM t_langues NATURAL JOIN t_cours WHERE id_crs = \"" . $_SESSION['sel_crs'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecole-facile :: <?php echo $crs_info[0]['nom_crs'] . " > " . $thm_info[0]['nom_thm'] ?></title>
        <!-- Bootstrap core CSS -->
        <link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./css/index.css" rel="stylesheet">
        <link href="<?php echo ROOT; ?>/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" media="screen">
        <script src="<?php echo ROOT; ?>./plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <ul class="nav nav-pills pull-right">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./logout.php">Déconnexion</a></li>
                </ul>
                <h3 class="text-muted"><?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></h3>
            </div>
            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Mot en <?php echo $langue[0]['nom_lng'];?></th>
                            <th>Mot en français</th>
                            <th>Sens</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $voc = liste_voc($pdo, $_SESSION['sel_thm']);
                            echo $voc;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
