<?php
session_start();
require("./../config/config.php");
include ("./class/class.inc.php");
include ("./secure.inc.php");
include ("fun.inc.php");
$pdo = new Mypdo;
if (isset($_POST['creer'])) {
    $ifexist = $pdo->query("SELECT * FROM t_themes WHERE nom_thm = \"" . $_POST['nom'] . "\" AND id_crs = \"" . $_POST['cours'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
    print_r($ifexist);
    if ($ifexist == null) {
        $pdo->query("INSERT INTO `t_themes` (`id_thm`, `nom_thm`, `description_thm`, `id_usr`, `id_crs`) VALUES (NULL, \"" . $_POST['nom'] . "\", \"" . $_POST['description'] . "\", \"" . $_SESSION['id'] . "\", \"" . $_POST['cours'] . "\")");
        header('Location: index.php');
    } else {
        $msg_err = true;
    }
}
?>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecole-facile :: Ajouter un thème</title>
        <!-- Bootstrap core CSS -->
        <link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./css/index.css" rel="stylesheet">
        <link href="<?php echo ROOT; ?>/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" media="screen">
        <script src="<?php echo ROOT; ?>./plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <ul class="nav nav-pills pull-right">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./logout.php">Déconnexion</a></li>
                </ul>
                <h3 class="text-muted"><?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></h3>
            </div>
            <div class="row">
                <?php
                if ($msg_err == true) {
                    echo "Ce cours éxiste déjà!!!";
                }
                ?>
                <fieldset>
                    <legend>Ajouter un thème</legend>
                    <form role="form" method="post" action="ajter_theme.php">
                        <div class="form-group">
                            <label for="nom">Nom : </label>
                            <input type="text" name="nom" id="nom" class='form-control'>
                        </div>
                        <div class="form-group">
                            <label for="description">Déscription : </label>
                            <textarea name="description" id="description" class='form-control'></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cours">Cours : </label>
                            <select name="cours" id="cours" class='form-control'>
                                <?php
                                $cours = option_cours($pdo);
                                echo $cours;
                                ?>
                            </select>
                        </div>
                        <input type="submit" value="Créer" name="creer" class='btn btn-default'>
                    </form>
                </fieldset>
            </div>
            <div class="footer">
                <p><!--&copy;--> Portail <?php echo NAME; ?> - Version <?php /* echo git_version(); */ ?></p>
            </div>
        </div>
    </body>
</html>
