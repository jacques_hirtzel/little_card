<?php
session_start();
require("./../config/config.php");
include ("./class/class.inc.php");
include ("./secure.inc.php");
include ("fun.inc.php");
$pdo = new Mypdo();
$boolean_mot1 = false;
$boolean_mot2 = false;
$choix_sens = false;
if (isset($_POST['select_sens'])) {
    if (isset($_POST['sens_check'])) {
        $pdo->query("INSERT INTO `t_sens` (`id_sns`, `id_mot_1`, `id_mot_2`, `sens_sens`) VALUES (NULL, \"" . $_POST['mot1'] . "\", \"" . $_POST['mot2'] . "\", \"" . $_POST['sens'] . "\")");
        $id_sens = $pdo->query("SELECT * FROM t_sens WHERE sens_sens = \"" . $_POST['sens'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
        $pdo->query("INSERT INTO `t_vocabulaires` (`id_voc`, `id_thm`, `id_sns`) VALUES (NULL, \"" . $_POST['theme'] . "\", \"" . $id_sens[0]['id_sns'] . "\")");
        header('Location: index.php');
    }else{
        $pdo->query("INSERT INTO `t_vocabulaires` (`id_voc`, `id_thm`, `id_sns`) VALUES (NULL, \"" . $_POST['theme'] . "\", \"" . $_POST['sens_ex'] . "\")");
        header('Location: index.php');
    }
} else if (isset($_POST['creer'])) {
    $ifexist = $pdo->query("SELECT * FROM t_sens AS s NATURAL JOIN t_vocabulaires AS v JOIN t_mots AS m ON m.id_mot = s.id_mot_1 WHERE v.id_thm =\"" . $_POST['theme'] . "\" AND m.mot_mot =\"" . $_POST['mot1'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
    if ($ifexist == null) {
        $mot1_exist = $pdo->query("SELECT * FROM t_mots WHERE mot_mot = \"" . $_POST['mot1'] . "\" AND id_lng=\"" . $_POST['mot1_lng'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
        if ($mot1_exist == null) {
            $pdo->query("INSERT INTO `t_mots` (`id_mot`, `mot_mot`, `id_lng`) VALUES (NULL, \"" . $_POST['mot1'] . "\", \"" . $_POST['mot1_lng'] . "\")");
            $boolean_mot1 = true;
        }
        $mot2_exist = $pdo->query("SELECT * FROM t_mots WHERE mot_mot = \"" . $_POST['mot2'] . "\" AND id_lng=\"1\"")->fetchAll(PDO::FETCH_ASSOC);
        if ($mot2_exist == null) {
            $pdo->query("INSERT INTO `t_mots` (`id_mot`, `mot_mot`, `id_lng`) VALUES (NULL, \"" . $_POST['mot2'] . "\", \"1\")");
            $boolean_mot2 = true;
        }
        $id_mot1 = $pdo->query("SELECT * FROM t_mots WHERE mot_mot = \"" . $_POST['mot1'] . "\" AND id_lng=\"" . $_POST['mot1_lng'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
        $id_mot2 = $pdo->query("SELECT * FROM t_mots WHERE mot_mot = \"" . $_POST['mot2'] . "\" AND id_lng=\"1\"")->fetchAll(PDO::FETCH_ASSOC);
        $theme = $_POST['theme'];
        if ($boolean_mot2 == true) {
            $pdo->query("INSERT INTO `t_sens` (`id_sns`, `id_mot_1`, `id_mot_2`, `sens_sens`) VALUES (NULL, \"" . $id_mot1[0]['id_mot'] . "\", \"" . $id_mot2[0]['id_mot'] . "\", \"" . $_POST['sens'] . "\")");
            $id_sens = $pdo->query("SELECT * FROM t_sens WHERE sens_sens = \"" . $_POST['sens'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
            $pdo->query("INSERT INTO `t_vocabulaires` (`id_voc`, `id_thm`, `id_sns`) VALUES (NULL, \"" . $_POST['theme'] . "\", \"" . $id_sens[0]['id_sns'] . "\")");
            header('Location: index.php');
        } else {
            $choix_sens = true;
        }
    } else {
        $msg_err = true;
    }
}
?>
<!DOCTYPE html>
<html>  
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecole-facile :: Ajouter une langue</title>
        <!-- Bootstrap core CSS -->
        <link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./css/index.css" rel="stylesheet">
        <link href="<?php echo ROOT; ?>/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" media="screen">
        <script src="<?php echo ROOT; ?>./plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="header">
                <ul class="nav nav-pills pull-right">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./logout.php">Déconnexion</a></li>
                </ul>
                <h3 class="text-muted"><?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></h3>
            </div>
            <div class="row">
                <?php
                if ($msg_err == true) {
                    echo "Ce mot éxiste déjà!!!";
                }
                ?>
                <fieldset>
                    <?php
                    if ($choix_sens == true) {
                        echo "<legend>Sélectionner ou créer un sens</legend>\n";
                        echo "\t\t\t<form role=\"form\" method=\"post\" action=\"ajter_mot.php\">\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"sens_ex\">Sens éxistant : </label>\n";
                        echo "\t\t\t\t\t<select name=\"sens_ex\" id=\"sens_ex\" class=\"form-control\">\n";
                        $cours = option_sens($pdo);
                        echo $cours;
                        echo "\t\t\t\t\t</select>\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"sens\">Créer le sens \"" . $_POST['sens'] . "\": </label>\n";
                        echo "\t\t\t\t\t<input type=\"checkbox\" name=\"sens_check\">\n";
                        echo "\t\t\t\t\t<input type=\"hidden\" name=\"sens\" value=\"" . $_POST['sens'] . "\">\n";
                        echo "\t\t\t\t\t<input type=\"hidden\" name=\"mot1\" value=\"" . $id_mot1[0]['id_mot'] . "\">\n";
                        echo "\t\t\t\t\t<input type=\"hidden\" name=\"mot2\" value=\"" . $id_mot2[0]['id_mot'] . "\">\n";
                        echo "\t\t\t\t\t<input type=\"hidden\" name=\"theme\" value=\"" . $theme . "\">\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<input type=\"submit\" name=\"select_sens\" value=\"Sélectionner\" class=\"btn btn-default\">\n";
                        echo "\t\t\t</form>\n";
                    } else if (!isset($_POST['select_cours'])) {
                        echo "<legend>Sélectionner un cours</legend>\n";
                        echo "\t\t\t<form role=\"form\" method=\"post\" action=\"ajter_mot.php\">\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"cours\">Cours : </label>\n";
                        echo "\t\t\t\t\t<select name=\"cours\" id=\"cours\" class=\"form-control\">\n";
                        $cours = option_cours($pdo);
                        echo $cours;
                        echo "\t\t\t\t\t</select>\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<input type=\"submit\" name=\"select_cours\" value=\"Sélectionner\" class=\"btn btn-default\">\n";
                        echo "\t\t\t</form>\n";
                    } else {
                        echo "<legend>Ajouter un mot</legend>\n";
                        echo "\t\t\t<form role=\"form\" method=\"post\" action=\"ajter_mot.php\">\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"theme\">Thème : </label>\n";
                        echo "\t\t\t\t\t<select name=\"theme\" id=\"theme\" class=\"form-control\">\n";
                        $theme = option_theme($pdo, $_POST['cours']);
                        echo $theme;
                        echo "\t\t\t\t\t</select>\n";
                        echo "\t\t\t\t</div>\n";
                        $id_crs = $pdo->query("SELECT * FROM t_cours WHERE id_crs = \"" . $_POST['cours'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
                        $langue = $pdo->query("SELECT * FROM t_langues WHERE id_lng = \"" . $id_crs[0]['id_lng'] . "\"")->fetchAll(PDO::FETCH_ASSOC);
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"mot1\">Mot en " . $langue[0]['nom_lng'] . " : </label>\n";
                        echo "\t\t\t\t\t<input type=\"text\" name=\"mot1\" id=\"mot1\" class=\"form-control\">\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"mot2\">Mot en français : </label>\n";
                        echo "\t\t\t\t\t<input type=\"text\" name=\"mot2\" id=\"mot2\" class=\"form-control\">\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<div class=\"form-group\">\n";
                        echo "\t\t\t\t\t<label for=\"sens\">Sens : </label>\n";
                        echo "\t\t\t\t\t<textarea name=\"sens\" id=\"sens\" class=\"form-control\"></textarea>\n";
                        echo "\t\t\t\t</div>\n";
                        echo "\t\t\t\t<input type=\"hidden\" name=\"mot1_lng\" value=\"" . $langue[0]['id_lng'] . "\">\n";
                        echo "\t\t\t\t<input type=\"submit\" name=\"creer\" value=\"Créer\" class=\"btn btn-default\">\n";
                        echo "\t\t\t</form>\n";
                    }
                    ?>
                </fieldset>
            </div>
            <div class="footer">
                <p><!--&copy;--> Portail <?php echo NAME; ?> - Version <?php /* echo git_version(); */ ?></p>
            </div>
        </div>
    </body>
</html>
