<?php
/* * *
  NE PAS OUBLIER DE MODIFIER LE FICHIER .HTACCESS
 * * */

session_start();
//print_r($_SESSION);
if (!isset($_SESSION['id']) || $_SESSION['ip_addr'] != $_SERVER["REMOTE_ADDR"]) {
    header('Location: ./login.php');
}
/* * *************************************************************** */
require("./../config/config.php");
include ("./class/class.inc.php");
include ("./secure.inc.php");
include ("fun.inc.php");

$user = new user($_SESSION['id']);
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Ecole-facile :: Accueil</title>
        <!-- Bootstrap core CSS -->
        <link href="./plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="./css/index.css" rel="stylesheet">
        <link href="<?php echo ROOT; ?>/plugins/jquery-ui/jquery-ui.css" rel="stylesheet" media="screen">
        <script src="<?php echo ROOT; ?>./plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo ROOT; ?>./plugins/jquery-ui/jquery-ui.min.js"></script>
    </head>
    <body>    
        <div class="container">
            <div class="header">
                <ul class="nav nav-pills pull-right">
                    <li><a href="./index.php">Accueil</a></li>
                    <li><a href="./logout.php">Déconnexion</a></li>
                </ul>
                <h3 class="text-muted"><?php echo $_SESSION['nom'] . " " . $_SESSION['prenom']; ?></h3>
            </div>
            <div class="row">
                <fieldset>
                    <legend><h2>Ajouts</h2></legend>
                    <div class="form-group">
                        <form method="post" action="ajter_cours.php">
                            <input type="submit" value="Ajouter un cours" name="ajter_cours" class='btn btn-default'>
                        </form>
                    </div>
                    <div class="form-group">
                        <form method="post" action="ajter_theme.php">
                            <input type="submit" value="Ajouter un thème" name="ajter_theme" class='btn btn-default'>
                        </form>
                    </div>
                    <div class="form-group">
                        <form method="post" action="ajter_langue.php">
                            <input type="submit" value="Ajouter une langue" name="ajter_langue" class='btn btn-default'>
                        </form>
                    </div>
                    <div class="form-group">
                        <form method="post" action="ajter_mot.php">
                            <input type="submit" value="Ajouter un mot" name="ajter_mot" class='btn btn-default'>
                        </form>
                    </div>         
                </fieldset>	
            </div>	<!-- row -->
            <div class="row">
                <h2>Liste des cours</h2>
                <ul class="list-group">
                    <?php
                    $liste_crs = liste_crs($pdo, $_SESSION['id']);
                    echo $liste_crs;
                    ?>
                </ul>
            </div>
            <div class="footer">
                <p><!--&copy;--> Portail <?php echo NAME; ?> - Version <?php /* echo git_version(); */ ?></p>
            </div>
        </div> <!-- /container -->
    </body>
</html>